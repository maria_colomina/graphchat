package com.mcolomina.graphchat.main.mvp

import android.view.View
import com.mcolomina.graphchat.AllChatsQuery
import com.mcolomina.graphchat.RealtimeSubscription.Chat
import io.reactivex.Observable

interface MainView {
  fun getView(): View
  fun setData(it: MutableList<AllChatsQuery.AllChat>?)
  fun addMessage(message: Chat)
  fun getNewMessage(): String
  fun sendClicks(): Observable<Unit>
  fun clearInput()
}