package com.mcolomina.graphchat.main.mvp.impl

import com.apollographql.apollo.api.Response
import com.mcolomina.graphchat.AllChatsQuery
import com.mcolomina.graphchat.PostChatMutation
import com.mcolomina.graphchat.RealtimeSubscription.Data
import com.mcolomina.graphchat.main.mvp.MainModel
import com.mcolomina.graphchat.network.NetworkManager
import io.reactivex.Flowable
import io.reactivex.Observable
import javax.inject.Inject

class MainModelImpl @Inject constructor(private val networkManager: NetworkManager) : MainModel {

  override fun getAllMessages(): Observable<MutableList<AllChatsQuery.AllChat>> {
    return networkManager.getAllChats()
        .map { it -> it.data()!!.allChats() }
  }

  override fun subscriptionRealTime(): Flowable<Response<Data>> {
    return networkManager.subscriptionRealTime()
  }

  override fun postChat(message: String): Observable<Response<PostChatMutation.Data>> {
    return networkManager.postChat(message)
  }

}