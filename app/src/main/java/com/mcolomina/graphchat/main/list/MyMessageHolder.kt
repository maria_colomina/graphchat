package com.mcolomina.graphchat.main.list

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.mcolomina.graphchat.AllChatsQuery
import com.mcolomina.graphchat.R

class MyMessageHolder(val view: View) : RecyclerView.ViewHolder(view) {

  private val text = view.findViewById<TextView>(R.id.textMessage)

  fun bind(item: AllChatsQuery.AllChat) {
    text.setText(item.contents())
  }
}