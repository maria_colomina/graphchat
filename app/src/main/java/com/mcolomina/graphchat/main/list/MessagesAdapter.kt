package com.mcolomina.graphchat.main.list

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.LayoutInflater
import android.view.ViewGroup
import com.mcolomina.graphchat.AllChatsQuery.AllChat
import com.mcolomina.graphchat.R
import com.mcolomina.graphchat.RealtimeSubscription.Chat
import com.mcolomina.graphchat.utils.getDeviceId
import com.mcolomina.graphchat.utils.toArrayList

class MessagesAdapter : RecyclerView.Adapter<ViewHolder>() {

  private val MY_MESSAGE = 0
  private val THEIR_MESSAGE = 1

  private val messages = ArrayList<AllChat>()

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    if (viewType == MY_MESSAGE)
      return MyMessageHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_my_message, parent, false))
    else return TheirMessageHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_their_message, parent, false))
  }

  override fun getItemCount(): Int {
    return messages.size
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    if (getItemViewType(position) == MY_MESSAGE) (holder as MyMessageHolder).bind(getItem(position))
    else (holder as TheirMessageHolder).bind(getItem(position))
  }

  private fun getItem(position: Int): AllChat {
    return messages[position]
  }

  fun setData(list: MutableList<AllChat>?) {
    messages.clear()
    messages.addAll(list!!.toArrayList())
    notifyDataSetChanged()
  }

  fun addMessage(message: Chat) {
    messages.add(AllChat(message.node()!!.__typename(), message.node()!!.contents(), message.node()!!.authorID()))
    notifyDataSetChanged()
    notifyItemInserted(messages.size)
  }

  override fun getItemViewType(position: Int): Int {
    if (getItem(position).authorID().equals(getDeviceId())) return MY_MESSAGE
    else return THEIR_MESSAGE
  }
}

