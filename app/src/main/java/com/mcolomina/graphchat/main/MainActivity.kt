package com.mcolomina.graphchat.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.mcolomina.graphchat.app.ChatApp
import com.mcolomina.graphchat.main.di.DaggerMainComponent
import com.mcolomina.graphchat.main.di.MainModule
import com.mcolomina.graphchat.main.mvp.MainModel
import com.mcolomina.graphchat.main.mvp.MainPresenter
import com.mcolomina.graphchat.main.mvp.MainView
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

  @Inject
  lateinit var view: MainView
  @Inject
  lateinit var model: MainModel
  @Inject
  lateinit var presenter: MainPresenter

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    DaggerMainComponent.builder()
        .mainModule(MainModule())
        .appComponent(ChatApp.getComponent())
        .build()
        .inject(this)

    setContentView(view.getView())
    presenter.onCreate()
  }

  override fun onDestroy() {
    super.onDestroy()
    presenter.onDestroy()
  }
}
