package com.mcolomina.graphchat.main.mvp.impl

import android.util.Log
import com.apollographql.apollo.api.Response
import com.mcolomina.graphchat.RealtimeSubscription
import com.mcolomina.graphchat.RealtimeSubscription.Data
import com.mcolomina.graphchat.main.mvp.MainModel
import com.mcolomina.graphchat.main.mvp.MainPresenter
import com.mcolomina.graphchat.main.mvp.MainView
import com.mcolomina.graphchat.utils.RxSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subscribers.DisposableSubscriber
import javax.inject.Inject

class MainPresenterImpl @Inject constructor(private val view: MainView, private val model: MainModel,
    private val schedulers: RxSchedulers) :
    MainPresenter {

  private val compositeDisposable = CompositeDisposable()

  override fun onCreate() {
    compositeDisposable.add(getAllMessagesSubscription())
    compositeDisposable.add(realTimeSubscription())
    compositeDisposable.add(postChatSubscription())
  }


  private fun getAllMessagesSubscription(): Disposable {
    return model.getAllMessages()
        .observeOn(schedulers.androidUI())
        .doOnNext { list ->
          view.setData(list)
        }
        .subscribe({}, { e -> Log.e("MYERROR", e.toString()) })
  }

  private fun realTimeSubscription(): Disposable {
    return model.subscriptionRealTime()
        .subscribeOn(schedulers.io())
        .observeOn(schedulers.androidUI())
        .subscribeWith(object : DisposableSubscriber<Response<RealtimeSubscription.Data>>() {
          override fun onNext(response: Response<Data>?) {
            Log.v("MESSAGEADDED", response!!.data()!!.Chat().toString())
            view.addMessage(response!!.data()!!.Chat()!!)
          }

          override fun onComplete() {
            Log.v("MESSAGEADDED", "FAILED - COMPLETE")
          }

          override fun onError(t: Throwable?) {
            Log.v("MESSAGEADDED", "FAILED - ERROR " + t.toString())
          }

        })
  }

  private fun postChatSubscription(): Disposable {
    return view.sendClicks()
        .observeOn(schedulers.io())
        .flatMap { model.postChat(view.getNewMessage()) }
        .observeOn(schedulers.androidUI())
        .doOnNext { view.clearInput() }
        .subscribe()
  }


  override fun onDestroy() {
    compositeDisposable.dispose()
  }
}