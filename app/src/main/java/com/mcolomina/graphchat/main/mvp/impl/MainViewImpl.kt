package com.mcolomina.graphchat.main.mvp.impl

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.TextView
import com.jakewharton.rxbinding2.view.clicks
import com.mcolomina.graphchat.AllChatsQuery.AllChat
import com.mcolomina.graphchat.R
import com.mcolomina.graphchat.RealtimeSubscription.Chat
import com.mcolomina.graphchat.main.list.MessagesAdapter
import com.mcolomina.graphchat.main.mvp.MainView
import javax.inject.Inject

class MainViewImpl @Inject constructor(context: Context) : MainView, FrameLayout(context) {

  private val messagesList: RecyclerView
  private val adapter: MessagesAdapter
  private val writeMessage: TextView
  private val buttonSend: ImageButton


  init {
    inflate(context, R.layout.activity_main, this)
    messagesList = findViewById<RecyclerView>(R.id.messagesList)
    writeMessage = findViewById<TextView>(R.id.editText)
    buttonSend = findViewById<ImageButton>(R.id.buttonSend)
    adapter = MessagesAdapter()
    messagesList.adapter = adapter
    messagesList.layoutManager = LinearLayoutManager(this.context)
    messagesList.setHasFixedSize(true);
  }

  override fun getView(): View = this

  override fun setData(list: MutableList<AllChat>?) {
    adapter.setData(list)
    messagesList.smoothScrollToPosition(adapter.itemCount)
  }

  override fun addMessage(message: Chat) {
    adapter.addMessage(message)
    messagesList.smoothScrollToPosition(adapter.itemCount)
  }

  override fun getNewMessage(): String = writeMessage.text.toString()

  override fun sendClicks() = buttonSend.clicks()

  override fun clearInput() = writeMessage.setText("")

}