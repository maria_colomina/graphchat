package com.mcolomina.graphchat.main.mvp

import com.apollographql.apollo.api.Response
import com.mcolomina.graphchat.AllChatsQuery.AllChat
import com.mcolomina.graphchat.PostChatMutation
import com.mcolomina.graphchat.RealtimeSubscription.Data
import io.reactivex.Flowable
import io.reactivex.Observable

interface MainModel {
  fun getAllMessages(): Observable<MutableList<AllChat>>
  fun subscriptionRealTime(): Flowable<Response<Data>>
  fun postChat(message: String): Observable<Response<PostChatMutation.Data>>
}