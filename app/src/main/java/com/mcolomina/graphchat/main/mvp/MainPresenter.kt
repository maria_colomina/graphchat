package com.mcolomina.graphchat.main.mvp

interface MainPresenter {

  fun onCreate ()
  fun onDestroy()
}