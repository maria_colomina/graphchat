package com.mcolomina.graphchat.main.di

import android.content.Context
import com.mcolomina.graphchat.app.AppComponent
import com.mcolomina.graphchat.main.MainActivity
import com.mcolomina.graphchat.main.mvp.MainModel
import com.mcolomina.graphchat.main.mvp.MainPresenter
import com.mcolomina.graphchat.main.mvp.MainView
import com.mcolomina.graphchat.main.mvp.impl.MainModelImpl
import com.mcolomina.graphchat.main.mvp.impl.MainPresenterImpl
import com.mcolomina.graphchat.main.mvp.impl.MainViewImpl
import com.mcolomina.graphchat.network.NetworkManager
import com.mcolomina.graphchat.utils.RxSchedulers
import dagger.Component
import dagger.Module
import dagger.Provides
import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.BINARY)
annotation class MainScope

@MainScope
@Component(modules = arrayOf(MainModule::class), dependencies = arrayOf(AppComponent::class))
interface MainComponent {
  fun inject(activity: MainActivity)
}

@Module
class MainModule {

  @Provides
  @MainScope
  fun provideModel(networkManager: NetworkManager): MainModel = MainModelImpl(networkManager)

  @Provides
  @MainScope
  fun provideView(context: Context): MainView = MainViewImpl(context)

  @Provides
  @MainScope
  fun providePresenter(model: MainModel, view: MainView, rxSchedulers: RxSchedulers): MainPresenter = MainPresenterImpl(view,
      model, rxSchedulers)

}