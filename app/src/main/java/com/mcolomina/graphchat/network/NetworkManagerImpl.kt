package com.mcolomina.graphchat.network

import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.Input
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.rx2.Rx2Apollo
import com.mcolomina.graphchat.AllChatsQuery
import com.mcolomina.graphchat.AllChatsQuery.Data
import com.mcolomina.graphchat.PostChatMutation
import com.mcolomina.graphchat.RealtimeSubscription
import com.mcolomina.graphchat.utils.getDeviceId
import io.reactivex.Flowable
import io.reactivex.Observable

class NetworkManagerImpl(private val apolloClient: ApolloClient) : NetworkManager {

  override fun getAllChats(): Observable<Response<Data>> {
    val call = apolloClient.query(AllChatsQuery())
    return Rx2Apollo.from(call)
  }

  override fun subscriptionRealTime(): Flowable<Response<RealtimeSubscription.Data>> {
    val call = apolloClient.subscribe(RealtimeSubscription(Input.optional("")))
    return Rx2Apollo.from(call)
  }

  override fun postChat(message: String): Observable<Response<PostChatMutation.Data>> {
    val call = apolloClient.mutate(PostChatMutation(message, getDeviceId()))
    return Rx2Apollo.from(call)
  }

}