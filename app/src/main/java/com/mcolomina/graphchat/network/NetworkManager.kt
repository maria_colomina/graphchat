package com.mcolomina.graphchat.network

import com.apollographql.apollo.api.Response
import com.mcolomina.graphchat.AllChatsQuery
import com.mcolomina.graphchat.PostChatMutation
import com.mcolomina.graphchat.RealtimeSubscription.Data
import io.reactivex.Flowable
import io.reactivex.Observable

interface NetworkManager {

  fun getAllChats(): Observable<Response<AllChatsQuery.Data>>
  fun subscriptionRealTime(): Flowable<Response<Data>>
  fun postChat(message: String): Observable<Response<PostChatMutation.Data>>
}