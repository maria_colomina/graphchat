package com.mcolomina.graphchat.utils

import android.provider.Settings
import com.mcolomina.graphchat.app.ChatApp

fun <E> MutableList<E>.toArrayList(): ArrayList<E> {
  val arrayList = ArrayList<E>()
  for (chat in this) {
    arrayList.add(chat)
  }
  return arrayList
}

fun getDeviceId(): String = Settings.Secure.getString(ChatApp.app.contentResolver,
    Settings.Secure.ANDROID_ID) ?: ""