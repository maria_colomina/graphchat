package com.mcolomina.graphchat.app

import android.content.Context
import com.mcolomina.graphchat.app.modules.AppModule
import com.mcolomina.graphchat.app.modules.NetworkModule
import com.mcolomina.graphchat.app.modules.RxModule
import com.mcolomina.graphchat.network.NetworkManager
import com.mcolomina.graphchat.utils.RxSchedulers
import dagger.Component
import dagger.android.AndroidInjector
import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.BINARY)
annotation class AppScope

@AppScope
@Component(modules = arrayOf(
    AppModule::class,
    NetworkModule::class,
    RxModule::class))
interface AppComponent : AndroidInjector<ChatApp> {

  fun context(): Context
  fun networkManager(): NetworkManager
  fun schedulers(): RxSchedulers
}
