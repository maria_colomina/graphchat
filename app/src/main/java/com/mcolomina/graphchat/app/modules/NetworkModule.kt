package com.mcolomina.graphchat.app.modules

import android.content.Context
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.Operation
import com.apollographql.apollo.api.ResponseField
import com.apollographql.apollo.cache.normalized.CacheKey
import com.apollographql.apollo.cache.normalized.CacheKeyResolver
import com.apollographql.apollo.cache.normalized.NormalizedCacheFactory
import com.apollographql.apollo.cache.normalized.lru.EvictionPolicy
import com.apollographql.apollo.cache.normalized.lru.LruNormalizedCache
import com.apollographql.apollo.cache.normalized.lru.LruNormalizedCacheFactory
import com.apollographql.apollo.cache.normalized.sql.ApolloSqlHelper
import com.apollographql.apollo.cache.normalized.sql.SqlNormalizedCacheFactory
import com.apollographql.apollo.subscription.WebSocketSubscriptionTransport
import com.mcolomina.graphchat.app.AppScope
import com.mcolomina.graphchat.network.NetworkManager
import com.mcolomina.graphchat.network.NetworkManagerImpl
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY

@Module
class NetworkModule {

  private val BASE_URL = "https://api.graph.cool/simple/v1/cjldia9nh08f3010737ay17ua"
  private val BASE_URL_SUBSCRIPTIONS = "wss://subscriptions.graph.cool/v1/cjldia9nh08f3010737ay17ua"
  private val SQL_CACHE_NAME = "graphChat"

  @Provides
  @AppScope
  fun provideOkHttp(interceptor: HttpLoggingInterceptor): OkHttpClient {
    return OkHttpClient.Builder()
        .addInterceptor(interceptor.setLevel(BODY))
        .build()
  }


  @Provides
  @AppScope
  fun provideInterceptor(): HttpLoggingInterceptor {
    return HttpLoggingInterceptor()
  }

  @Provides
  @AppScope
  fun provideNormalizedCacheFactory(context: Context): NormalizedCacheFactory<LruNormalizedCache> {
    val apolloSqlHelper = ApolloSqlHelper(context, SQL_CACHE_NAME)
    return LruNormalizedCacheFactory(EvictionPolicy.NO_EVICTION)
        .chain(SqlNormalizedCacheFactory(apolloSqlHelper))
  }

  @Provides
  @AppScope
  fun provideCacheKeyResolver(): CacheKeyResolver {
    val cacheKeyResolver = object : CacheKeyResolver() {
      override fun fromFieldRecordSet(field: ResponseField, recordSet: Map<String, Any>): CacheKey {
        val typeName = recordSet["__typename"] as String
        if ("User" == typeName) {
          val userKey = typeName + "." + recordSet["login"]
          return CacheKey.from(userKey)
        }
        if (recordSet.containsKey("id")) {
          val typeNameAndIDKey = recordSet["__typename"].toString() + "." + recordSet["id"]
          return CacheKey.from(typeNameAndIDKey)
        }
        return CacheKey.NO_KEY
      }

      // Use this resolver to customize the key for fields with variables: eg entry(repoFullName: $repoFullName).
      // This is useful if you want to make query to be able to resolved, even if it has never been run before.
      override fun fromFieldArguments(field: ResponseField, variables: Operation.Variables): CacheKey {
        return CacheKey.NO_KEY
      }
    }

    return cacheKeyResolver
  }


  @Provides
  @AppScope
  fun provideNetworkManager(apolloClient: ApolloClient): NetworkManager {
    return NetworkManagerImpl(apolloClient)
  }

  @Provides
  @AppScope
  fun provideApolloClient(okHttpClient: OkHttpClient, normalizedCacheFactory: NormalizedCacheFactory<LruNormalizedCache>,
      cacheKeyResolver: CacheKeyResolver):
      ApolloClient {
    return ApolloClient.builder()
        .serverUrl(BASE_URL)
        .okHttpClient(okHttpClient)
        .normalizedCache(normalizedCacheFactory, cacheKeyResolver)
        .subscriptionTransportFactory(WebSocketSubscriptionTransport.Factory(BASE_URL_SUBSCRIPTIONS, okHttpClient))
        .build()

  }

}