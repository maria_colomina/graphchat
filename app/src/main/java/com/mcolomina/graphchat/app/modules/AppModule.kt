package com.mcolomina.graphchat.app.modules

import android.content.Context
import com.mcolomina.graphchat.app.AppScope
import com.mcolomina.graphchat.app.ChatApp
import dagger.Module
import dagger.Provides

@Module
class AppModule(private val app: ChatApp) {

  @Provides
  @AppScope
  fun provideContext(): Context = app.applicationContext

}