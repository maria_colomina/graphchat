package com.mcolomina.graphchat.app

import android.app.Application
import com.mcolomina.graphchat.app.modules.AppModule

class ChatApp : Application() {
  companion object {
    fun getComponent(): AppComponent = app.appComponent

    lateinit var app: ChatApp
      private set
  }

  private lateinit var appComponent: AppComponent

  override fun onCreate() {
    super.onCreate()
    app = this

    appComponent = DaggerAppComponent.builder()
        .appModule(AppModule(this))
        .build()

  }


}