package com.mcolomina.graphchat.app.modules

import com.mcolomina.graphchat.app.AppScope
import com.mcolomina.graphchat.utils.RxSchedulers
import dagger.Module
import dagger.Provides

@Module
class RxModule {

  @Provides
  @AppScope
  fun provideSchedulers(): RxSchedulers = RxSchedulers()
}